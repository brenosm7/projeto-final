package com.connectdf.projetoconnect.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.connectdf.projetoconnect.model.Produto;

/* Avisando para o Spring que essa é uma classe de reposiório 
 * O Spring devolve o controle e uma instancia pronta do 
 * repositorio
*/

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Integer>{

}


