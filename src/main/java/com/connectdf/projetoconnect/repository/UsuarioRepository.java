package com.connectdf.projetoconnect.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.connectdf.classesId.UsuarioId;
import com.connectdf.projetoconnect.model.Usuario;

/**
 * Para usar chave composta é necessario criar uma classe com
 * os campo da chave e passar ela por parametro
 */
public interface UsuarioRepository extends JpaRepository<Usuario, UsuarioId> {

    @Query("SELECT u from Usuario u "
            + " where u.usuarioId.id = :id"
            + " and u.usuarioId.codEmpresa = :codEmpresa")
    Usuario findByIdAndCodEmpresa(Integer id, Integer codEmpresa);

    // @Query("DELETE from Usuario"
    // + " where usuarioId.id = :id"
    // + " and usuarioId.codEmpresa = :codEmpresa")
    // Usuario deleteByIdAndCodEmpresa(Integer id, Integer codEmpresa);

    // @Transactional
    // @Query("UPDATE Usuario "
    // + " set usuario.nome = :nome"
    // + " , usuario.txEmail = :txEmail"
    // + " , usuario.dtNasc = :dtNasc"
    // + " where usuarioId.id = :id"
    // + " and usuarioId.codEmpresa = :codEmpresa")
    // @Modifying
    // //Usuario updateUsuario(@Param("id") Integer id, @Param("codEmpresa") Integer
    // codEmpresa, @Param("nome") String nome, @Param("txEmail") String txEmail,
    // @Param("dtNasc") String dtNasc);
    // Usuario updateUsuario(Integer id, Integer codEmpresa, String nome, String
    // txEmail, String dtNasc);
}
