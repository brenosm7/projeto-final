package com.connectdf.projetoconnect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoConnectApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoConnectApplication.class, args);
	}

}
