package com.connectdf.projetoconnect.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.connectdf.classesId.UsuarioId;
import com.connectdf.projetoconnect.model.Usuario;
import com.connectdf.projetoconnect.repository.UsuarioRepository;;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario adicionar(Usuario usuario) {
        return usuarioRepository.save(usuario);
    }

    /**
     * @return
     */
    public List<Usuario> obterTodos() {
        // regra de negocio caso tenha
        return usuarioRepository.findAll();
    }

    public Usuario obterPorId(UsuarioId usuarioId) {
        return usuarioRepository.findByIdAndCodEmpresa(usuarioId.getId(), usuarioId.getCodEmpresa());
    }

    public Usuario atualizar(Integer id, Integer codEmpresa, Usuario usuario) {
        usuario.getUsuarioId().setId(id);
        usuario.getUsuarioId().setCodEmpresa(codEmpresa);
        return usuarioRepository.save(usuario);

    }

    // public void deletar(UsuarioId usuarioId) {
    // usuarioRepository.deleteByIdAndCodEmpresa(usuarioId.getId(),
    // usuarioId.getCodEmpresa());
    // }
}
