package com.connectdf.projetoconnect.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.connectdf.projetoconnect.model.Produto;
import com.connectdf.projetoconnect.repository.ProdutoRepository;

/**
 * Anotação Service para informar ao Spring que é classe de
 * serviço
 * Anotação Autowired para não precisar instanciar e recuperar
 * do metodo do repositorio
 */
@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public List<Produto> obterTodos() {
        // regra de negocio caso tenha
        return produtoRepository.findAll();
    }

    public Optional<Produto> obterPorId(Integer id) {
        
        return produtoRepository.findById(id);

    }

    public Produto adicionar(Produto produto) {

        produto.setId(null);
        return produtoRepository.save(produto);

    }

    public void deletar(Integer id){
        produtoRepository.deleteById(id);
    }

    public Produto atualizar(Integer id, Produto produto){
        //recebo id pela Url
        produto.setId(id);
        return produtoRepository.save(produto);


    }
}
