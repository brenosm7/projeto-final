package com.connectdf.projetoconnect.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.connectdf.classesId.UsuarioId;
import com.connectdf.projetoconnect.model.Usuario;
import com.connectdf.projetoconnect.services.UsuarioService;

@RestController
@RequestMapping("/api/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    public Usuario adicionar(@RequestBody Usuario usuario) {
        return usuarioService.adicionar(usuario);
    }

    @GetMapping("/listarTodos")
    public List<Usuario> obterTodos() {
        return usuarioService.obterTodos();
    }

    @GetMapping
    public Usuario obterporId(@RequestBody UsuarioId usuarioId) {
        return usuarioService.obterPorId(usuarioId);
    }

    @CrossOrigin(origins = "*")
    @PutMapping("/altera/{id}/{codEmpresa}")
    public Usuario atualizar(@RequestBody Usuario usuario, @PathVariable Integer id, @PathVariable Integer codEmpresa) {
        return usuarioService.atualizar(id, codEmpresa, usuario);
    }

    // @DeleteMapping("/deletar")
    // public String deletar(@RequestBody UsuarioId usuarioId){
    // usuarioService.deletar(usuarioId);
    // return "Produto com id: " + usuarioId + " Deletado com sucesso!";
    // }
}
