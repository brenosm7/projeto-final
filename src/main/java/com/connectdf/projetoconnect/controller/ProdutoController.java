package com.connectdf.projetoconnect.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.connectdf.projetoconnect.services.ProdutoService;
import com.connectdf.projetoconnect.model.Produto;

/**
 * Anotação Controller - informar para Spring que é uma cla
 * classe controller
 * Anotação GetMapping informar o verbo da ação, get = buscar.
 * Anotação RequestBody para trasnformar a requisição no objeto
 * Produto
 * Anotação PostMapping - informando para o Spring que é um Post
 * Anotação PathVariable - Pega a variavel que esta no caminho
 * para uso
 */
/* ... */

@RestController
@RequestMapping("/api/produtos")
@CrossOrigin(origins = "*")

public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @GetMapping
    public List<Produto> obterTodos() {
        return produtoService.obterTodos();
    }

    @GetMapping("/{id}")
    public Optional<Produto> obterporId(@PathVariable Integer id) {
        return produtoService.obterPorId(id);
    }

    @PostMapping
    public Produto adicionar(@RequestBody Produto produto) {
        return produtoService.adicionar(produto);

    }

    @DeleteMapping("/{id}")
    public String deletar(@PathVariable Integer id) {
        produtoService.deletar(id);
        return "Produto com id: " + id + " Deletado com sucesso!";
    }

    @PutMapping("/{id}")
    public Produto atualizar(@RequestBody Produto produto, @PathVariable Integer id) {
        return produtoService.atualizar(id, produto);

    }

}
